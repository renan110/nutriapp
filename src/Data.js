export const navLinksFr = [
    {"id": 1, "name": "IMC", "description": "Calcul ton IMC"},
    {"id": 2, "name": "NutriScore", "description": "Analyse un aliment"},
    {"id": 3, "name": "Mes repas", "description": "Gère tes repas"},
];

export const navLinksEn = [
    {"id": 1, "name": "BMI", "description": "Calculate your BMI"},
    {"id": 2, "name": "NutriScore", "description": "Analyze food"},
    {"id": 3, "name": "My meals", "description": "Manage your meal"},
];