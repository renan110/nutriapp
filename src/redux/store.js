import { configureStore } from '@reduxjs/toolkit';
import dataReducer from './dataSlice';

// Création du store Redux
const store = configureStore({
  reducer: {
    data: dataReducer,
  },
});

export default store;
