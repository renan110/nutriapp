import { createSlice } from '@reduxjs/toolkit';

const dataSlice = createSlice({
  name: 'data',
  initialState: {
    products: null, // Initialiser à null ou à un tableau vide selon vos besoins
  },
  reducers: {
    setData: (state, action) => {
      state.products = action.payload;
    },
  },
});

export const { setData } = dataSlice.actions;
export default dataSlice.reducer;
