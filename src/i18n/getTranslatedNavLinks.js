import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';

const TranslatedNavLinks = () => {
  const { t, i18n } = useTranslation();

  useEffect(() => {
    // Force la réexécution de TranslatedNavLinks lorsque la langue change
  }, [i18n.language]);

  const translatedNavLinks = [
    { id: 1, name: t('common.BMI'), description: t('common.calculBMI') },
    { id: 2, name: t('common.nutriScore'), description: t('common.analyzefood') },
    { id: 3, name: t('common.myMeals'), description: t('common.manageMeals') },
  ];

  return translatedNavLinks;
};

export default TranslatedNavLinks;
