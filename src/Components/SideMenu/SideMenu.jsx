import React, { useState } from "react";
import { navLinks } from "../Data";
import './SideMenu.css';

const SideMenu = () => {
  const [isOpen, setIsOpen] = useState(false);

  const toggleMenu = () => {
    setIsOpen(!isOpen);
  };

  return (
    <div>
      <button onClick={toggleMenu}>X</button>

      {navLinks.map((link) => {
        return (
          <div className="link" key={link.id}>
            <p>{link.description}</p>
          </div>
        );
      })}
    </div>
  );
};

export default SideMenu;
