import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import SearchMenu from "./SearchMenu";
import "../css/FoodCard.css";
import NutritionalValues from "./NutritionalValues";

const FoodCard = (props) => {
  const { t } = useTranslation();
  const currentLanguage = t("common.translate-button");

  const handleImageError = (event) => {
    event.target.src = "https://www.bricozor.com/static/img/visuel-indisponible-650.png";
  };

  const getColorNutriGrade = (grade) => {
    if (grade === "a") {
      return "greenDark";
    } else if (grade === "b") {
      return "greenLight";
    } else if (grade === "c") {
      return "yellow";
    } else if (grade === "d") {
      return "orange";
    } else if (grade === "e") {
      return "red";
    }
  };

  const toUpperCaseVerif = (string) => {
    if (string != null && string != undefined) {
      return string.toUpperCase();
    } else {
      return t("common.noData");
    }
  };

  return (
    <div className="productCardCtn">
      <div id="productName" className="fontWeightBold textAlignCenter mB8">
        {props.product.product_name_fr}
      </div>

      <div className="imgCtn textAlignCenter">
      {props.product.image_url != undefined ? (
        <img
          className="imgProduct"
          src={props.product.image_url}
          alt={t("common.noImage")}
          onError={handleImageError}
        />
      ) : (
        <img
          className="imgProduct"
          src="https://www.bricozor.com/static/img/visuel-indisponible-650.png"
          alt={t("common.noImage")}
        />
      )}
      </div>

      <NutritionalValues 
        key={props.product.code}
        code={props.product.code}
      />
    </div>
  );
  
};

export default FoodCard;
