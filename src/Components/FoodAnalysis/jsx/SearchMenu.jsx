import React, { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import "../css/SearchMenu.css";
import FoodCard from "./FoodCard.jsx";
import { useSelector, useDispatch } from "react-redux";
import { setData } from "../../../redux/dataSlice.js";

const SearchMenu = () => {
  const { t } = useTranslation();
  const currentLanguage = t("common.translate-button");

  const [searchValue, setSearchValue] = useState("");
  const nutritionData = useSelector((state) => state.data);
  const [productNotFound, setProductNotFound] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [nbPages, setNbPages] = useState(0);
  const [visiblePages, setVisiblePages] = useState([]);

  console.log(visiblePages);

  const dispatch = useDispatch();

  // Fonction de gestion de la recherche
  const handleFetch = async (page) => {
    window.scrollTo(0, 0);
    // Mettre anglais / francais
    try {
      console.log("click1");
      const response = await fetch(
        `https://world.openfoodfacts.org/cgi/search.pl?search_terms=${searchValue}&search_simple=1&action=process&page=${page}&json=1`
      );
      const data = await response.json();
      dispatch(setData(data.products));
      setNbPages(Math.ceil(data.count / 24));

      // Calculer la plage de pages à afficher
      let startPage = page - 2;
      let endPage = page + 2;

      // Gérer les cas spéciaux
      if (startPage <= 0) {
        startPage = 1;
        endPage = Math.min(5, nbPages);
      }

      if (endPage > nbPages) {
        endPage = nbPages;
        startPage = Math.max(1, nbPages - 4);
      }

      // Mettre à jour les pages visibles
      const newVisiblePages = Array.from(
        { length: endPage - startPage + 1 },
        (_, i) => startPage + i
      );
      setVisiblePages(newVisiblePages);
    } catch (error) {
      console.error("Erreur lors de la recherche :", error);
      setProductNotFound(true);
    }
  };

  const handleSearch = async () => {
    setCurrentPage(1);
    handleFetch(1);
  };

  const handleNextPage = async () => {
    if (currentPage < nbPages) {
      setCurrentPage(currentPage + 1);
      handleFetch(currentPage + 1);
    }
  };

  const handlePreviousPage = async () => {
    if (currentPage > 1) {
      setCurrentPage(currentPage - 1);
      handleFetch(currentPage - 1);
    }
  };

  const handleChosenPage = async (chosenPage) => {
    if (chosenPage >= 0 && chosenPage <= nbPages) {
      setCurrentPage(chosenPage);
      handleFetch(chosenPage);
    }
  };

  const getVisiblePageNumbers = () => {
    const totalPages = nbPages;
    const currentPageNum = currentPage;
    const visiblePageNumbers = [];
  
    const totalVisiblePages = 9; // Le nombre total de boutons visibles
    const numStartEndButtons = 3; // Le nombre de boutons pour les pages 1, 2 et 3 et les 3 dernières pages
  
    // Cas où il n'y a pas assez de pages pour afficher les points de suspension
    if (totalPages <= totalVisiblePages) {
      for (let i = 1; i <= totalPages; i++) {
        visiblePageNumbers.push(i);
      }
    } else {
      // Ajouter les boutons pour les pages 1, 2 et 3
      for (let i = 1; i <= numStartEndButtons; i++) {
        visiblePageNumbers.push(i);
      }
  
      const middleStart = Math.max(numStartEndButtons + 1, currentPageNum - Math.floor(totalVisiblePages / 2));
      const middleEnd = Math.min(totalPages - numStartEndButtons, currentPageNum + Math.floor(totalVisiblePages / 2));
  
      if (middleStart > numStartEndButtons + 1) {
        visiblePageNumbers.push('...');
      }
  
      for (let i = middleStart; i <= middleEnd; i++) {
        visiblePageNumbers.push(i);
      }
  
      if (middleEnd < totalPages - numStartEndButtons) {
        visiblePageNumbers.push('...');
      }
  
      // Ajouter les boutons pour les 3 dernières pages
      for (let i = totalPages - numStartEndButtons + 1; i <= totalPages; i++) {
        visiblePageNumbers.push(i);
      }
    }
  
    return visiblePageNumbers;
  };

  useEffect(() => {
    if (nutritionData) {
      setProductNotFound(
        !nutritionData.products || nutritionData.products.length === 0
      );
    }
  }, [nutritionData, currentPage]);

  return (
    <div>
      <input
        type="text"
        value={searchValue}
        onChange={(e) => setSearchValue(e.target.value)}
      />
      <button onClick={() => handleSearch()}>{t("common.search")}</button>

      <div className="cardsCtn">
        {!productNotFound &&
          nutritionData &&
          nutritionData.products &&
          nutritionData.products.map((product) => (
            <FoodCard key={product.code} product={product} />
          ))}
        {productNotFound && <p>{t("common.noProductFound")}</p>}
      </div>

      <div className="pagination">
        {/* Bouton pour la page précédente */}
        <button onClick={handlePreviousPage}>{"<<"}</button>

        {/* Afficher les numéros de page */}
        {getVisiblePageNumbers().map((pageNum, index, arr) => {
          // Afficher le bouton de la page actuelle
          if (pageNum === currentPage) {
            return (
              <button
                key={pageNum}
                onClick={() => handleChosenPage(pageNum)}
                className="active"
              >
                {pageNum}
              </button>
            );
          }

          // Afficher les points de suspension entre deux boutons de page
          if (index > 0 && arr[index - 1] !== pageNum - 1) {
            return (
              <span
                key={`ellipsis-${pageNum}`}
                className="fontWeightBold flexHorizontal alignItemCenter"
              >
                . . .
              </span>
            );
          }

          // Afficher les autres numéros de page
          return (
            <button key={pageNum} onClick={() => handleChosenPage(pageNum)}>
              {pageNum}
            </button>
          );
        })}

        {/* Bouton pour la page suivante */}
        <button onClick={handleNextPage}>{">>"}</button>
      </div>
    </div>
  );
};

export default SearchMenu;
