import React, { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import { useSelector, useDispatch } from "react-redux";
import { setData } from "../../../redux/dataSlice.js";
import "../../../App.css";
import "../css/NutritionalValues.css";
import data from "../dataFoodAnalysis.js";

const NutritionalValues = (props) => {
  const { t } = useTranslation();
  const products = useSelector((state) => state.data.products);

  const product = products?.find((product) => product.code === props.code);

  const renderNutriment = (nutrimentType) => {
    const nutrimentData = data[nutrimentType];
    const nutrimentAPIKeys = nutrimentData.nutrimentAPI;
    const suffixes = nutrimentData.suffix;
    const color = nutrimentData.color[0];

    const formattedNutriments = nutrimentAPIKeys.map(
      (nutrimentAPIKey, index) => {
        const value = product.nutriments[nutrimentAPIKey];
        const suffix = suffixes[index];

        const formattedValue = value ? `${value} ${suffix}` : "X";

        return (
          <span key={index}>
            {index !== 0 && " / "}
            {formattedValue}
          </span>
        );
      }
    );

    return (
      <div
        className={`flexHorizontalCenter gap10 values spaceBetween ${color} p4`}
      >
        <div className="label">{t(`common.${nutrimentType}`)}</div>
        <div className="nutrimentsValues">{formattedNutriments}</div>
      </div>
    );
  };

  return (
    <div className="nutritionValuesCtn flexVerticalCenter br8 p8 fontWeightBold">
      <div className="br4 overflowHidden">
        <div
          id="labelAgvNV"
          className="p4 black br4 overflowHidden textAlignCenter flexHorizontalCenter alignItemCenter"
        >
          {t("common.AvgNV")} {t("common.for100g")}
        </div>

        {Object.keys(data).map((nutrimentType) =>
          renderNutriment(nutrimentType)
        )}

      </div>
      <div id="legend" className="mT8">
        X = {t("common.noData")}
      </div>
    </div>
  );
};

export default NutritionalValues;
