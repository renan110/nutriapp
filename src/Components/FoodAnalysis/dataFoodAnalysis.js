const data = {
    energy: {
        nutrimentAPI: ['energy-kj_100g', 'energy-kcal_100g'],
        suffix: ['kj ', 'kcal'],
        color: ['green']
    },
    totalFat: {
        nutrimentAPI: ['fat_100g'],
        suffix: ['g'],
        color: ['brown']
    },
    saturedFat: {
        nutrimentAPI: ['saturated-fat_100g'],
        suffix: ['g'],
        color: ['brownLight']
    },
    totalCarbohydrates: {
        nutrimentAPI: ['carbohydrates_100g'],
        suffix: ['g'],
        color: ['pink']
    },
    sugars: {
        nutrimentAPI: ['sugars_100g'],
        suffix: ['g'],
        color: ['pinkLight']
    },
    protein: {
        nutrimentAPI: ['proteins_100g'],
        suffix: ['g'],
        color: ['red']
    },
    salt: {
        nutrimentAPI: ['salt_100g'],
        suffix: ['g'],
        color: ['blue']
    },
};
  
export default data;

