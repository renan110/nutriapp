import React, { useState } from "react";
import Lang from "../Langage/Lang.tsx";
import { useTranslation } from "react-i18next";
import { navLinksFr, navLinksEn } from "../../Data.js";
import "./NavBar.css";
import "../../App.css";

const NavBar = () => {
  const { t } = useTranslation();
  const currentLanguage = t("common.translate-button");

  let navLinks = navLinksFr;
  if (currentLanguage === "FR") {
    navLinks = navLinksFr;
  } else if (currentLanguage === "EN") {
    navLinks = navLinksEn;
  }

  return (
    <div className="ctn">
      <div className="linksCtn">
        {navLinks.map((link) => {
          return (
            <div className="link pointer" key={link.id}>
              <p>{link.name}</p>
            </div>
          );
        })}
      </div>

      <div className="languagesCtn">
        <Lang />
      </div>
    </div>
  );
};

export default NavBar;
