import React, { useState } from "react";
import Lang from "../Langage/Lang.tsx";
import { useTranslation } from "react-i18next";
import imcImg from "../../res/img/imc.jpg";
import bmiImg from "../../res/img/bmi.jpg";
import "./ImcCalculator.css";

const ImcCalculator = () => {
  const { t } = useTranslation();
  const currentLanguage = t("common.translate-button");

  let imgElement = null;

  if (currentLanguage === "FR") {
    imgElement = <img className="bmiImg" src={imcImg} alt="Image IMC" />;
  } else if (currentLanguage === "EN") {
    imgElement = <img className="bmiImg" src={bmiImg} alt="Image BMI" />;
  }

  const [weight, setWeight] = useState("");
  const [heightCm, setHeightCm] = useState("");
  const [imc, setImc] = useState(null);
  const [entryError, setEntryError] = useState(false);
  const [imcColor, setImcColor] = useState("");

  const calculateImc = () => {
    const weightInKg = parseFloat(weight);
    const heightInM = parseFloat(heightCm) / 100;

    if (weightInKg > 1 && weightInKg < 300 && heightInM > 1 && heightCm < 250) {
      const imcValue = weightInKg / (heightInM * heightInM);
      setImc(imcValue.toFixed(2));
      setEntryError(false);

      if (imcValue < 18.5 && currentLanguage === "FR") {
        setImcColor("blueFR");
        return;
      } else if (imcValue < 18.5 && currentLanguage === "EN") {
        setImcColor("blueEN");
        return;
      } else if (imcValue < 24.9) {
        setImcColor("green");
        return;
      } else if (imcValue < 29.9) {
        setImcColor("yellow");
        return;
      } else if (imcValue < 34.9) {
        setImcColor("orange");
        return;
      } else if (imcValue > 35) {
        setImcColor("red");
        return;
      }
    } else {
      setImc(null);
      setEntryError(true);
    }
  };

  return (
    <div className="ctnIMC">
      <h2>{t("common.BmiCalculator")}</h2>

      <div className="ctnContentIMC">
        {imgElement}
        <div className="ctnFormIMC">
          <div className="entryIMC">
            <label className="labelIMC">{t("common.weight")}:</label>
            <input
              type="number"
              value={weight}
              onChange={(e) => setWeight(e.target.value)}
            />
          </div>
          <div>
            <div className="entryIMC">
              <label className="labelIMC">{t("common.height")}:</label>
              <input
                type="number"
                value={heightCm}
                onChange={(e) => setHeightCm(e.target.value)}
              />
            </div>
          </div>
          <button onClick={calculateImc}>{t("common.calculate")}</button>
          {imc && (
            <p className={imcColor}>
              {t("common.yourBmi")} {imc}
            </p>
          )}

          {entryError && (
            <div className="entryError">{t("common.entryError")}</div>
          )}
        </div>
      </div>
    </div>
  );
};

export default ImcCalculator;
