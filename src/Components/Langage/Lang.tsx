import React, { ChangeEvent, useState } from "react"
import { useTranslation } from 'react-i18next';
import { Language } from "../../enums/Language.ts";
import './Lang.css';
import '../../App.css';

import FlagIcon from "../FlagIcon/FlagIcon.js";

 
const Lang = () => {

    const { i18n } = useTranslation();
    const [lang, setLang] = useState<Language>(i18n.language as Language);
 
    let changeLanguage = (language: string) => {
      
        switch (language) {
            case Language.EN:
                setLang(Language.EN);
                i18n.changeLanguage(Language.EN);
                break;
            case Language.FR:
              setLang(Language.FR);
              i18n.changeLanguage(Language.FR);
              break;
        }
    }
 
  
    return (
      <div className="ctn">

        <div className="pointer" onClick={() => changeLanguage("fr")}>
        <FlagIcon code="fr"/>
        </div>

        <div className="pointer" onClick={() => changeLanguage("en")}>
        <FlagIcon code="gb"/>
        </div>

      </div>
  )
}
 
export default Lang;